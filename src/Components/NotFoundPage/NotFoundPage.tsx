import * as React from 'react';
import './NotFoundPage.scss';

class NotFoundPage extends React.Component {

  public render() {
    return (
      <div className="NotFoundPage">
        Not found
      </div>
    );
  }
}

export default NotFoundPage;