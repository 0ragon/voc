import * as React from 'react';
import './SideMenu/SideMenu';
import './Main.scss';
import SideMenu from './SideMenu/SideMenu';

class Main extends React.Component {

  public paragraphClickHandler(): void {
    alert('Click!');
  }

  public render() {
    return (
      <div className="main-container">
          <SideMenu/>
           Main
      </div>
    );
  }
}

export default Main;
