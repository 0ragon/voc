import * as React from 'react';
import './Auth.scss';
import SignIn from '@Components/Auth/SignIn/SignIn';
import SignUp from '@Components/Auth/SignUp/SignUp';
import { BrowserRouter, Route, Switch, Link, Redirect, NavLink } from 'react-router-dom';

class Auth extends React.Component {


  public render() {
    return (
      <div className="container-fluid center">
        <div className="auth-container center flex-column col-xs-12 col-md-8">
          <div className="auth__tabs d-flex">
            <NavLink to="/auth/sign-in" className="auth__tab" activeClassName="tabSelected">
              <div >
                Sign in
                </div>
            </NavLink>
            <NavLink to="/auth/sign-up" className="auth__tab" activeClassName="tabSelected">
              Sign Up
            </NavLink>
          </div>
          <div className="auth__main full-width">
            <Switch>
              <Route path='/auth/sign-in' component={SignIn} />
              <Route path='/auth/sign-up' component={SignUp} />
              <Route path='*' component={SignIn} />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default Auth;