import * as React from 'react';
import './Header.scss';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
// import {Tabs, Tab} from '@material-ui/core';

interface State {
  selectedTabIndex: number;
}

class Header extends React.Component<any, any> {
  menuItems = [
    { name: 'Language', route: '/language' },
    { name: 'Scheduler', route: '/scheduler' },
    { name: 'Music', route: '/music' },
    { name: 'Articles', route: '/articles' },
  ];

  constructor(props: Object) {
    super(props);
    this.state = {
      selectedTabIndex: 0,
    };
  }


  onTabSelect(index: number): void {
    this.setState({
      selectedTabIndex: index
    });
  }

  onClickHandler = (): void => {
  }

  public render() {
    return (
      <AppBar>
        <Toolbar>
          <Tabs value={this.state.selectedTabIndex}>
            {
              this.menuItems.map((item, index) => {
                return (
                    <Tab label={item.name} key={item.name} onClick={this.onTabSelect.bind(this, index)} containerelement={<Link to="/first"/>}/>
                )
              })
            }
          </Tabs>
        </Toolbar>
      </AppBar>
    )
    //   <div className="header__container">

    //     {this.menuItems.map(item => {
    //       return (
    //         <Button>{item}</Button>
    //       )
    //     })}
    //     <Link to="/auth">Header</Link>
    //     <Link to="/loc">Local</Link>
    //     <div>
    //       <button onClick={this.onClickHandler} className="btn btn-danger">Press</button>
    //     </div>
    //   </div>
    // );
  }
}

export default Header;
