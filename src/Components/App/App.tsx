import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Link, Redirect } from 'react-router-dom';

import Auth from '@Components/Auth/Auth';
import Header from '@Components/Header/Header';
import * as Main from '@Components/Main';
import NotFoundPage from '@Components/NotFoundPage/NotFoundPage';
import './App.scss';

class App extends React.Component {
  isAuthorized = false;

  public render() {

    return (
      <div className="app-container">
        <BrowserRouter>
          {/* <Redirect to={this.isAuthorized ? '/main' : '/auth'} /> */}
          <Route path="/auth" component={Auth} />
          {/* <Switch>
            <Route path="/main" component={Main} />
            <Route path="/auth" component={Auth} />
            <Route component={NotFoundPage} />
          </Switch> */}
        </BrowserRouter>
      </div >
    );
  }
}

export default App;
