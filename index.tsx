import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './src/Components/App/App';
import { Provider } from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './src/Reducers';
import './style.scss';

const store = createStore(reducers, {}, applyMiddleware());

ReactDOM.render(<Provider store={store}>
                    <App/>
                </Provider>, document.getElementById('root'));
