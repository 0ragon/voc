const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');
const webpack = require('webpack')


function srcPath(subdir) {
    return path.join(__dirname, "src", subdir);
}

module.exports = {
    entry: './index.tsx',
    mode: 'development',
    devtool: 'inline-source-map',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        // hot: true,
        overlay: true,
        open: false,
        port: 8000,
        historyApiFallback: true,
    },
    // files don't need type specification when import
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"],
        alias: {
            '@Components': srcPath('Components'),
        },
    },
    module: {
        rules: [
            {
                test: /\.(jsx?)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(tsx?)$/,
                use: [{
                    loader: 'awesome-typescript-loader',
                    options: {
                        useBabel: true,
                        useCash: true,
                        transpileOnly: true
                    }
                }]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    /*
                        create separate css file per js whick contains CSS,
                        used only on production builds without style-loader in the loaders chain, 
                        doesn't support HMR
                    */
                    // MiniCssExtractPlugin.loader
                    {
                        loader: 'css-loader',
                        options: {
                            // url: true,
                            // import: true,
                            // modules: true,
                            // importLoaders: 1,
                            // localIdentName: "[name]_[local]_[hash:base64]",
                            sourceMap: true
                        }
                    },
                    "fast-sass-loader"
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            title: 'Vocabulary',
            cache: true
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css"
        }),
        new webpack.ProvidePlugin({
            lodash: 'lodash',
            _: 'underscore'
        }),
        new BundleAnalyzerPlugin()
    ],
}